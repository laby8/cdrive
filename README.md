CDrive is an application which is supposed to help people to drive safely on the road.
It's gathering geographical data about every user and checks if he is a danger to other users, based on his location, speed, and direction he is moving in.
Made with android studio using gradle, google firebase and google play services API.
The code could be much cleaner, all the location related code is kind of messy, also there could be another class used just for smartphone device stuff, like getting permissions, turning on location.

How it looks like: https://imgur.com/a/OJXB7b5