package com.project.igor.Cdrive;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.adam.kochamandroida.R;

import java.util.Observable;
import java.util.Observer;

public class CDriveService extends Service {
    static public Notification goodNote;
    static public Notification badNote;
    public static Observer counterObserver;
    private Locations locations;
    private User user;
    private PendingIntent snoozePendingIntent;
    private Boolean isLocation = false;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @para Used to name the worker thread, important only for debugging.
     */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras() != null) {
            destroy();
        }
        createNotificationChannel();
        initialize();
        startLocating();
        startForeground(1, goodNote);
        checkLocationEnabled();
        return Service.START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void checkLocationEnabled() {
        BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                } catch (Exception ex) {
                }
                if (intent.getAction().matches("android.location.PROVIDERS_CHANGED") && isLocation == false) {
                    isLocation = true;
                    startForeground(1, badNote);
                } else {
                    isLocation = false;
                    startForeground(1, goodNote);
                }
            }
        };
        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }


    private void createNotificationChannel() {
        Intent deleteIntent = new Intent(this, CDriveService.class).putExtra("destroy", "1");
        snoozePendingIntent =
                PendingIntent.getService(this, 1, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getString(R.string.channelId), "channel", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("cDriving");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);


        }
    }

    private void destroy() {
        user.stop();
        locations.stop();
        stopForeground(false);
        stopSelf();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void initialize() {
        counterObserver = (Observable o, Object arg) -> {
            createNotifications();
            startForeground(1, goodNote);
        };
        user = new User(getApplicationContext());
        locations = new Locations();
        createNotifications();
    }

    private void startLocating() {
        user.getLocation(locations, user);
        locations.getChildren(locations.getDataRef(), user);
        user.startLocationUpdates();
    }

    private void createNotifications() {
        goodNote = new NotificationCompat.Builder(getApplicationContext(), getString(R.string.channelId)).setSmallIcon(R.drawable.andy)
                .setContentTitle("Looking for danger")
                .setContentText("Uniknięto zagrożeń: " + user.getCounter()).addAction(R.drawable.andy, "STOP", snoozePendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT).build();
        badNote = new NotificationCompat.Builder(getApplicationContext(), getString(R.string.channelId)).setSmallIcon(R.drawable.andy)
                .setContentTitle("Turn on your localization")
                .setContentText("Uniknięto zagrożeń: " + user.getCounter()).addAction(R.drawable.andy, "STOP", snoozePendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT).build();
    }
}

