package com.project.igor.Cdrive;

public class BounceInterpolator implements android.view.animation.Interpolator {
    private double buttonAmplitude;
    private double buttonFrequency;

    BounceInterpolator(double amplitude, double frequency) {
        buttonAmplitude = amplitude;
        buttonFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time / buttonAmplitude) *
                Math.cos(buttonFrequency * time) + 1);
    }
}
