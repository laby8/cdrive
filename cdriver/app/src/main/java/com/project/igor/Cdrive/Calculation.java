package com.project.igor.Cdrive;

import java.util.function.BooleanSupplier;

public class Calculation {
    private static double myLat;
    private static double fLat;
    private static double myLong;
    private static double fLong;
    private static double angle;
    private static BooleanSupplier slowSpeedTest;
    private static BooleanSupplier distanceTest;

    public static boolean calculateDanger(CLocation myLocation, CLocation fLocation) {
        angle = Math.abs(fLocation.getBearing() - myLocation.getBearing());
        initTests(myLocation, fLocation);
        return slowSpeedTest.getAsBoolean();
    }

    private static void initTests(CLocation myLocation, CLocation fLocation) {
        slowSpeedTest = () -> (getDistance(myLocation, fLocation) <= 0.8 && ((angle <= 160 && angle >= 90) || (angle >= 200 && angle <= 270)) && (fLocation.getSpeed() >= 27.8 && myLocation.getSpeed() > 14));
        distanceTest = () -> getDistance(myLocation, fLocation) < 100000000;
    }

    public static double getDistance(CLocation myLocation, CLocation fLocation) {
        myLat = myLocation.getLatitude();
        fLat = fLocation.getLatitude();
        myLong = myLocation.getLongitude();
        fLong = fLocation.getLongitude();
        return Math.sqrt(Math.pow(fLat - myLat, 2)
                + Math.pow(Math.cos((myLat * Math.PI) / 180) * (fLong - myLong), 2)) * (40075.704 / 360);
    }


}
