package com.project.igor.Cdrive;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;

import com.example.adam.kochamandroida.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.Observable;


public class User extends Observable {
    public long time;
    private String userId;
    private Context context;
    private LocationCallback locationCallback;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient fusedLocationProvider;
    private MediaPlayer mp;
    private int counter;

    User(Context context) {
        mp = MediaPlayer.create(context, R.raw.sound);
        this.context = context;
        counter = 0;
        this.userId = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(this.context);
        addObserver(CDriveService.counterObserver);
    }

    public int getCounter() {
        return counter;
    }

    public void getLocation(final Locations locations, final User user) {
        setLocationRequest();
        startLocationCallBack(locations, user);
        startLocationUpdates();
    }

    public void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProvider.requestLocationUpdates(mLocationRequest, locationCallback, null);
        }
    }

    private void startLocationCallBack(final Locations locations, final User user) {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                for (Location location : locationResult.getLocations()) {
                    locations.getMyLocation().setCLocation(location);
                }
                locations.sendLocation(user);

            }
        };
    }

    private void setLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void alarmMe(boolean alarmDanger) {
        if (alarmDanger && (!mp.isPlaying())) {
            counter++;
            this.setChanged();
            this.notifyObservers(counter);
            time = System.nanoTime();
            mp.start();
        } else if (!alarmDanger && (mp.isPlaying()) && (System.nanoTime() - time > 5000)) {
            mp.pause();
        }


    }

    public String getUserId() {
        return userId;
    }

    public void stop() {
        fusedLocationProvider.removeLocationUpdates(locationCallback);
        locationCallback = null;
        mLocationRequest = null;
        fusedLocationProvider = null;
    }
}
