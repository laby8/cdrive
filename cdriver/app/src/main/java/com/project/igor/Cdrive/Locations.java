package com.project.igor.Cdrive;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Locations {
    private FirebaseDatabase database;
    private DatabaseReference dataRef;
    private CLocation myLocation;
    private ChildEventListener locationChangeListener;
    private CLocation post;

    Locations() {
        myLocation = new CLocation();
        database = FirebaseDatabase.getInstance();
        dataRef = database.getReference("Location");
    }

    public DatabaseReference getDataRef() {
        return dataRef;
    }

    public CLocation getMyLocation() {
        return myLocation;
    }

    void sendLocation(User user) {

        dataRef.child("" + user.getUserId()).setValue(myLocation);

    }

    public void getChildren(DatabaseReference ref, final User user) {

        ref.addChildEventListener(locationChangeListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (!user.getUserId().equals(dataSnapshot.getKey())) {
                    post = dataSnapshot.getValue(CLocation.class);
                    user.alarmMe(Calculation.calculateDanger(myLocation, post));
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void stop() {
        dataRef.removeEventListener(locationChangeListener);
        database = null;
        dataRef = null;
        myLocation = null;
        locationChangeListener = null;
    }

}

