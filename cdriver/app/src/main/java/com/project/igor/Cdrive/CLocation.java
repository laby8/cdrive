package com.project.igor.Cdrive;


import android.location.Location;

public class CLocation {
    private double longitude;
    private double bearing;
    private double speed;
    private double latitude;


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longtitude) {
        this.longitude = longtitude;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setCLocation(Location locationResult) {
        this.latitude = locationResult.getLatitude();
        this.bearing = locationResult.getBearing();
        this.longitude = locationResult.getLongitude();
        this.speed = locationResult.getSpeed();
    }

    @Override
    public String toString() {
        return "CLocation{" +
                "longitude=" + longitude +
                ", bearing=" + bearing +
                ", speed=" + speed +
                ", latitude=" + latitude +
                '}';
    }
}
