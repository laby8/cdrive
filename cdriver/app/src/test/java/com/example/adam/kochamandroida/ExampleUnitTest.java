package com.example.adam.kochamandroida;

import com.project.igor.Cdrive.CLocation;
import com.project.igor.Cdrive.Calculation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Mock
    Calculation calculation;

    @Mock
    CLocation cLocation1, cLocation2;




    @Before
    public void initilizeValues(){
        cLocation1 = new CLocation();
        cLocation2 = new CLocation();

    }

    @Test
    public void calculate_distance_with_the_same_value(){
        assertEquals(0,Calculation.getDistance(cLocation1,cLocation2),0.01);
    }

    @Test
    public void calculate_distance_with_diffrent_value(){

        cLocation2.setLatitude(50);
        cLocation2.setLongtitude(50);

        assertNotEquals(0,Calculation.getDistance(cLocation1,cLocation2),0.01);
    }

    @Test
    public void calculate_distance_with_diffrent_value2(){

        cLocation2.setLatitude(30.1);
        cLocation2.setLongtitude(8.3);

        assertNotEquals(0,Calculation.getDistance(cLocation1,cLocation2),0.01);
    }


    @Test
    public void calculate_danger_should_be_dangerous(){
        cLocation2.setBearing(180);
        cLocation1.setBearing(300);
        cLocation2.setSpeed(30);
        cLocation1.setSpeed(30);
        assertEquals(true, Calculation.calculateDanger(cLocation1,cLocation2));
    }

    @Test
    public void calculate_danger_should_not_be_dangerous_angles(){
        cLocation1.setBearing(360);
        cLocation2.setBearing(180);
        cLocation1.setSpeed(25);
        cLocation2.setSpeed(25);
        assertNotEquals(true, Calculation.calculateDanger(cLocation1, cLocation2));
    }

    @Test
    public  void calculate_danger_should_not_be_dangerous(){
        cLocation2.setBearing(180);
        cLocation1.setBearing(300);
        cLocation2.setSpeed(10);
        cLocation1.setSpeed(15);
        assertNotEquals(true, Calculation.calculateDanger(cLocation1,cLocation2));
    }

    @Test
    public void location1_getLatitude(){
        cLocation1.setLatitude(54.54355654);
        assertEquals(54.54355654, cLocation1.getLatitude(), 0.00001);
    }


    @Test
    public void location1_getBearing(){
        cLocation1.setBearing(164);
        assertEquals(164, cLocation1.getBearing(), 0.1);
    }


    @Test
    public void location2_getLongtitude(){
        cLocation2.setLongtitude(18.3254435346);
        assertEquals(18.32544353346, cLocation2.getLongitude(), 0.00001);
    }

    @Test
    public void location2_getSpeed(){
        cLocation2.setSpeed(24.56);
        assertNotEquals(24.80, cLocation2.getSpeed(), 0.1);
    }

    @Test
    public void string_output(){
        assertNotNull(cLocation1.toString());
    }

    @Test
    public void getDistance_check_distance(){
        cLocation1.setLatitude(54.57284527694911);
        cLocation1.setLongtitude(18.384826183319092);
        cLocation2.setLatitude(54.521305668721915);
        cLocation2.setLongtitude(18.529601097106934);
        assertEquals(10.98, Calculation.getDistance(cLocation1, cLocation2), 0.03);
    }
}